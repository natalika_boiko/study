 # Команды Git
 
 ### Версия
 ```bash
 git version
 ```
 
### Клонирование
```bash
git clone https://natalika_boiko@bitbucket.org/natalika_boiko/study.git
```

### Настройка Git
```bash
git config --global user.email natalika.boiko@icloud.com
git config --global user.name "Natallia Boiko"

``` 
### Состояние
```bash
git status
```
### Добавление файлов в отслеживание
```bash
git add .
```

### Удаление файлов из отслеживания
```bash
git rm --cashed <имя файла>
(файл не удаляется, остаётся на месте)
```
### Коммиты
```bash
```